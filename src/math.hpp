#pragma once

#include <array>
#include <vector>
#include <limits>
#include <memory>
#include <iostream>
#include <typeinfo>

#include "GLMatrix.hpp"
#include "GLPoint.hpp"
#include "GLVector.hpp"

inline GLVector operator*(const GLVector &lhs, double scale)
{
  return GLVector(lhs(0) * scale, lhs(1) * scale, lhs(2) * scale);
}

inline GLVector operator*(double scale, const GLVector &rhs)
{
  return GLVector(rhs(0) * scale, rhs(1) * scale, rhs(2) * scale);
}

inline GLPoint operator/(const GLVector &lhs, const GLVector &rhs)
{
  return GLPoint(lhs(0) / rhs(0), lhs(1) / rhs(1), lhs(2) / rhs(2));
}

inline GLPoint operator*(const GLPoint &lhs, double scale)
{
  return GLPoint(lhs(0) * scale, lhs(1) * scale, lhs(2) * scale);
}

inline GLPoint operator*(double scale, const GLPoint &rhs)
{
  return GLPoint(rhs(0) * scale, rhs(1) * scale, rhs(2) * scale);
}

inline GLPoint operator+(const GLPoint &p1, const GLVector &p2)
{
  return GLPoint(p1(0) + p2(0), p1(1) + p2(1), p1(2) + p2(2));
}

inline GLPoint operator+(const GLPoint &p1, const GLPoint &p2)
{
  return GLPoint(p1(0) + p2(0), p1(1) + p2(1), p1(2) + p2(2));
}

inline GLVector operator+(const GLVector &p1, const GLVector &p2)
{
  return GLVector(p1(0) + p2(0), p1(1) + p2(1), p1(2) + p2(2));
}

inline GLVector operator-(const GLPoint &p1, const GLPoint &p2)
{
  return GLVector(p1(0) - p2(0), p1(1) - p2(1), p1(2) - p2(2));
}

inline bool operator==(const GLVector &p1, const GLVector &p2)
{
  return (p1(0) == p2(0) && p1(1) == p2(1) && p1(2) == p2(2));
}

inline GLVector crossProduct(const GLVector &lhs, const GLVector &rhs)
{
  return GLVector(lhs(1) * rhs(2) - lhs(2) * rhs(1),
                  lhs(2) * rhs(0) - lhs(0) * rhs(2),
                  lhs(0) * rhs(1) - lhs(1) * rhs(0));
}

inline double dotProduct(const GLVector &lhs, const GLVector &rhs)
{
  return lhs(0) * rhs(0) + lhs(1) * rhs(1) + lhs(2) * rhs(2);
}

inline bool equal(const GLPoint &lhs, const GLPoint &rhs)
{
  return (lhs(0) == rhs(0) && lhs(1) == rhs(1) && lhs(2) == rhs(2));
}

inline double areaParalellogramm(GLPoint &first, GLPoint &second, GLPoint &third)
{
  GLVector AB = second - first;
  GLVector AC = third - first;
  return crossProduct((AB), (AC)).norm();
}
inline int sgn(int x) { return (x > 0) ? 1 : (x < 0) ? -1
                                                     : 0; }

/** Aufgabenblatt 2, Aufgabe 2 **/

inline GLVector operator*(const GLMatrix &lhs, const GLVector &rhs)
{
  double x = lhs(0, 0) * rhs(0) + lhs(0, 1) * rhs(1) + lhs(0, 2) * rhs(2) + lhs(0, 3) * 0;
  double y = lhs(1, 0) * rhs(0) + lhs(1, 1) * rhs(1) + lhs(1, 2) * rhs(2) + lhs(1, 3) * 0;
  double z = lhs(2, 0) * rhs(0) + lhs(2, 1) * rhs(1) + lhs(2, 2) * rhs(2) + lhs(2, 3) * 0;

  return GLVector(x, y, z);
}

inline GLPoint operator*(const GLMatrix &lhs, const GLPoint &rhs)
{
  double x = lhs(0, 0) * rhs(0) + lhs(0, 1) * rhs(1) + lhs(0, 2) * rhs(2) + lhs(0, 3) * 1;
  double y = lhs(1, 0) * rhs(0) + lhs(1, 1) * rhs(1) + lhs(1, 2) * rhs(2) + lhs(1, 3) * 1;
  double z = lhs(2, 0) * rhs(0) + lhs(2, 1) * rhs(1) + lhs(2, 2) * rhs(2) + lhs(2, 3) * 1;

  return GLPoint(x, y, z);
}

inline GLMatrix operator*(const GLMatrix &lhs, const GLMatrix &rhs)
{
  GLMatrix matrix;
  for (int ci = 0; ci <= 3; ci++)
  {
    // std::cout << ci << std::endl;

    for (int rowindex = 0; rowindex <= 3; ++rowindex)
    {
      // std::cout << rowindex << std::endl;
      float matrixentry = 0;
      for (int i = 0; i <= 3; ++i)
      {
        // std::cout << "rowindex"<< rowindex << "i"<<  i << std::endl;
        matrixentry += lhs(rowindex, i) * rhs(i, ci);
      }
      matrix.setValue(rowindex, ci, matrixentry);
    }
  }
  return matrix;
}

inline bool intersectAABB(GLPoint rayOrigin, GLVector rayDir, GLPoint boxMin, GLPoint boxMax)
{
  GLVector invDir = GLVector(1.0f / rayDir(0), 1.0f / rayDir(1), 1.0f / rayDir(2));

  float tMin = (boxMin(0) - rayOrigin(0)) * invDir(0);
  float tMax = (boxMax(0) - rayOrigin(0)) * invDir(0);

  if (invDir(0) < 0.0f)
    std::swap(tMin, tMax);

  float tYMin = (boxMin(1) - rayOrigin(1)) * invDir(1);
  float tYMax = (boxMax(1) - rayOrigin(1)) * invDir(1);

  if (invDir(1) < 0.0f)
    std::swap(tYMin, tYMax);

  if ((tMin > tYMax) || (tYMin > tMax))
    return false;

  if (tYMin > tMin)
    tMin = tYMin;

  if (tYMax < tMax)
    tMax = tYMax;

  float tZMin = (boxMin(2) - rayOrigin(2)) * invDir(2);
  float tZMax = (boxMax(2) - rayOrigin(2)) * invDir(2);

  if (invDir(2) < 0.0f)
    std::swap(tZMin, tZMax);

  if ((tMin > tZMax) || (tZMin > tMax))
    return false;

  return true;
}
