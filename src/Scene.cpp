#include "Scene.hpp"

#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <assimp/Importer.hpp>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <vector>

Scene::Scene() {}

/**
 * Gibt zurück ob ein gegebener Strahl ein Objekt (Modell oder Kugel) der Szene trifft
 *  (Aufgabenblatt 3)
 */
bool Scene::intersect(const Ray &ray, HitRecord &hitRecord, const float epsilon)
{
  bool ret_val = false;
  for (size_t sphereID = 0; sphereID < mSpheres.size(); ++sphereID)
  {
    Sphere sphere = mSpheres[sphereID];
    if (sphereIntersect(ray, sphere, hitRecord, epsilon))
    {
      hitRecord.sphereId = sphereID;

      // default werte falls vorher schon object getroffen wurden, die jetzt aber hinter der sphere sind
      hitRecord.modelId = -1;
      hitRecord.triangleId = -1;
      ret_val = true;
    }
  }

  // iterieren mit einfachen for-loop um später die IDs fürs shading zu erhalten
  for (size_t ModelID = 0; ModelID < mModels.size(); ++ModelID)
  {
    Model model = mModels[ModelID];
    if (!intersectAABB(ray.origin, ray.direction, model.mLowest, model.mHighest))
    {
      continue;
    }
    for (size_t TriangleID : model.findTriangles(ray, model.mOcttree))
    {
      Triangle tri = model.mTriangles[TriangleID];
      mTests += 1;
      if (triangleIntersect(ray, tri, hitRecord, epsilon))
      {
        hitRecord.modelId = ModelID;
        hitRecord.triangleId = TriangleID;

        // default wert falls vorher schon sphären getroffen wurden, die jetzt aber hinter dem Modell sind
        hitRecord.sphereId = -1;
        ret_val = true;
      }
    }
  }

  return ret_val;
}

/** Aufgabenblatt 3: Gibt zurück ob ein gegebener Strahl ein Dreieck  eines Modells der Szene trifft
 *  Diese Methode sollte in Scene::intersect für jedes Objektdreieck aufgerufen werden
 *  Aufgabenblatt 4: Diese Methode befüllt den den HitRecord im Fall eines Treffers mit allen für das shading notwendigen informationen
 */
bool Scene::triangleIntersect(const Ray &ray, const Triangle &triangle, HitRecord &hitRecord, const float epsilon)
{
  // Schnittpunktberechnung

  GLVector erster_spannvector = triangle.vertex[1] - triangle.vertex[0];
  GLVector zweiter_spannvector = triangle.vertex[2] - triangle.vertex[0];
  GLVector normalvector = crossProduct(erster_spannvector, zweiter_spannvector);
  // GLVector normalvector = triangle.normal;

  double oben = dotProduct((triangle.vertex[0] - ray.origin), normalvector);
  double unten = dotProduct(ray.direction, normalvector);

  // falls ebene quasi parallel zum strahl liegt
  if (unten < epsilon && unten > -epsilon)
  {
    return false;
  }

  double t = oben / unten;

  // falls schnittpunkt hinter der kamera liegt
  if (t < 0)
  {
    return false;
  }
  GLPoint schnittpunkt = ray.origin + t * ray.direction;
  double schnittdistanz = (schnittpunkt - ray.origin).norm();
  bool test_for_distance = (schnittdistanz > 0 && (hitRecord.parameter == -1 || schnittdistanz < hitRecord.parameter));
  if (!test_for_distance)
  {
    return false;
  }

  // Schnittpunktverifizierung
  double flächeninhalt_original = 0.5 * normalvector.norm();

  // Berechnen und aufsummieren von den 3 teildreiecken
  double flächeninhalt_neu = 0;
  for (int i = 0; i < 3; i++)
  {
    GLVector erster_spannvector_neu = triangle.vertex[i % 3] - schnittpunkt;
    GLVector zweiter_spannvector_neu = triangle.vertex[(i + 1) % 3] - schnittpunkt;
    GLVector normalvector_neu = crossProduct(erster_spannvector_neu, zweiter_spannvector_neu);
    flächeninhalt_neu += 0.5 * normalvector_neu.norm();
  }

  // wenn die Fläche gleich ist, dann ist der punkt drin, plus rundungsfehler
  if (flächeninhalt_neu <= flächeninhalt_original * (1.0 + epsilon * 10))
  {
    if (test_for_distance)
    {
      hitRecord.parameter = schnittdistanz;
      hitRecord.normal = triangle.normal;
      hitRecord.rayDirection = ray.direction;
      hitRecord.intersectionPoint = schnittpunkt;
      return true;
    }
  }
  return false;
}

bool Scene::triangleIntersectExperimental(const Ray &ray, const Triangle &triangle, HitRecord &hitRecord, const float epsilon)
{
  // Schnittpunktberechnung
  GLVector edge1 = triangle.vertex[1] - triangle.vertex[0];
  GLVector edge2 = triangle.vertex[2] - triangle.vertex[0];
  GLVector ray_cross_e2 = crossProduct(ray.direction, edge2);
  float det = dotProduct(edge1, ray_cross_e2);
  // GLVector normalvector = triangle.normal;

  // falls ebene quasi parallel zum strahl liegt
  if (det < epsilon && det > -epsilon)
  {
    return false;
  }

  float inv_det = 1.0 / det;
  GLVector s = ray.origin - triangle.vertex[0];
  float u = inv_det * dotProduct(s, ray_cross_e2);

  if (u < 0 || u > 1)
    return false;

  GLVector s_cross_e1 = crossProduct(s, edge1);
  float v = inv_det * dotProduct(ray.direction, s_cross_e1);

  if (v < 0 || u + v > 1)
    return false;

  // At this stage we can compute t to find out where the intersection point is on the line.
  float t = inv_det * dotProduct(edge2, s_cross_e1);

  if (t > epsilon) // ray intersection
  {
    GLPoint schnittpunkt = ray.origin + t * ray.direction;
    double schnittdistanz = (schnittpunkt - ray.origin).norm();
    bool test_for_distance = (schnittdistanz > 0 && (hitRecord.parameter == -1 || schnittdistanz < hitRecord.parameter));
    if (!test_for_distance)
    {
      return false;
    }
    hitRecord.parameter = schnittdistanz;
    hitRecord.normal = triangle.normal;
    hitRecord.rayDirection = ray.direction;
    hitRecord.intersectionPoint = schnittpunkt;
    return true;
  }
  else // This means that there is a line intersection but not a ray intersection.
    return false;
}

/** Aufgabenblatt 3: Gibt zurück ob ein gegebener Strahl eine Kugel der Szene trifft
 *  Diese Methode sollte in Scene::intersect für jede Kugel aufgerufen werden
 *  Aufgabenblatt 4: Diese Methode befüllt den den HitRecord im Fall eines Treffers mit allen für das shading notwendigen informationen
 */
bool Scene::sphereIntersect(const Ray &ray, const Sphere &sphere, HitRecord &hitRecord, const float epsilon)
{
  GLVector v = ray.direction;

  GLVector em = ray.origin - sphere.getPosition();
  double a = dotProduct(v, v);
  double b = 2 * dotProduct(v, em);
  double c = dotProduct(em, em) - sphere.getRadius() * sphere.getRadius();

  double Discriminant = b * b - 4 * a * c;

  double firstRoot;
  double secondRoot;

  // discriminant < 0, no target hit
  // discriminant = 1, ray is tanget to sphere
  // discriminant > 0, ray hits sphere twice
  if (Discriminant < 0)
  {
    return false;
  }
  else
  {
    firstRoot = (-b + std::sqrt(b * b - 4 * a * c)) / (2 * a);
    secondRoot = (-b - std::sqrt(b * b - 4 * a * c)) / (2 * a);
    double smallesRoot = std::min(firstRoot, secondRoot);
    double biggestRoot = std::max(firstRoot, secondRoot);
    if (firstRoot < 0 && secondRoot < 0)
    {
      return false;
    }
    else if (smallesRoot < 0 && biggestRoot > 0)
    {
      smallesRoot = biggestRoot;
    }

    GLPoint schnittpunkt = ray.origin + smallesRoot * ray.direction;
    double schnittdistanz = (schnittpunkt - ray.origin).norm();
    if (schnittdistanz > 0 && (hitRecord.parameter == -1 || schnittdistanz < hitRecord.parameter))
    {
      hitRecord.parameter = schnittdistanz;
      hitRecord.normal = schnittpunkt - sphere.getPosition();
      hitRecord.rayDirection = ray.direction;
      hitRecord.intersectionPoint = schnittpunkt;
      return true;
    }
  }
  return false;
}

/**
** Liest die Modelle (deren Dateinamen in pFiles stehen) ein und speichert sie
*in mModels
**/
void Scene::load(const std::vector<std::string> &pFiles)
{
  std::cout << "Laden der PLY Dateien:" << std::endl;
  // Für alle Objekte (Objekt meint hier das selbe wie Model)
  for (int obj_nr = 0; obj_nr < pFiles.size(); obj_nr++)
  {
    std::cout << "\tModel-Index: " << obj_nr << std::endl;
    // Assimp übernimmt das Einlesen der ply-Dateien
    Assimp::Importer importer;
    const aiScene *assimpScene = importer.ReadFile(
        pFiles[obj_nr], aiProcess_CalcTangentSpace | aiProcess_Triangulate |
                            aiProcess_JoinIdenticalVertices |
                            aiProcess_SortByPType);

    if (!assimpScene)
    {
      std::cout << importer.GetErrorString() << std::endl;
    }
    assert(assimpScene);
    auto meshes = assimpScene->mMeshes;
    // Neues Model erstellen
    Model model = Model();

    // Für alle Meshes des Models
    for (int i = 0; i < assimpScene->mNumMeshes; i++)
    {
      std::cout << "\t\tMesh-Index: " << i << " (" << meshes[i]->mNumFaces
                << " Faces)" << std::endl;
      auto faces = meshes[i]->mFaces;
      auto vertices = meshes[i]->mVertices;

      // Für alle Faces einzelner Meshes
      for (int j = 0; j < meshes[i]->mNumFaces; j++)
      {
        // Dreieck konstruieren und nötige Werte berechnen
        Triangle tri;
        assert(faces[j].mNumIndices == 3);
        tri.vertex[0] = GLPoint(vertices[faces[j].mIndices[0]].x,
                                vertices[faces[j].mIndices[0]].y,
                                vertices[faces[j].mIndices[0]].z);
        tri.vertex[1] = GLPoint(vertices[faces[j].mIndices[1]].x,
                                vertices[faces[j].mIndices[1]].y,
                                vertices[faces[j].mIndices[1]].z);
        tri.vertex[2] = GLPoint(vertices[faces[j].mIndices[2]].x,
                                vertices[faces[j].mIndices[2]].y,
                                vertices[faces[j].mIndices[2]].z);
        GLVector normal = crossProduct(tri.vertex[1] - tri.vertex[0],
                                       tri.vertex[2] - tri.vertex[0]);
        normal.normalize();
        tri.normal = normal;
        // Jedes Dreieck zum Vector der Dreiecke des aktuellen Models hinzufügen
        model.mTriangles.push_back(tri);
      }
    }
    // Immer das gleiche Material für das Model setzen
    Material material;
    material.color = Color(0.00, 1.00, 0.00);
    model.setMaterial(material);
    // Jedes Model zum Vector der Models der Scene hinzufügen
    mModels.push_back(model);
  }

  std::cout << "Laden der PLY Dateien abgeschlossen." << std::endl;
}

void Scene::setCamera(std::shared_ptr<Camera> cam) { mCamera = cam; }

std::shared_ptr<Camera> Scene::getCamera() const { return mCamera; }

GLPoint Scene::getViewPoint() const
{
  if (mCamera)
    return mCamera->getEyePoint();
  else
  {
    std::cerr << "No Camera set to get view point from." << std::endl;
    return GLPoint(0, 0, 0);
  }
}

void Scene::addPointLight(GLPoint pointLight)
{
  mPointLights.push_back(pointLight);
}

void Scene::addModel(Model model) { mModels.push_back(model); }

void Scene::addSphere(Sphere sphere) { mSpheres.push_back(sphere); }

std::vector<Model> &Scene::getModels() { return mModels; }

std::vector<Sphere> &Scene::getSpheres() { return mSpheres; }

std::vector<GLPoint> &Scene::getPointLights() { return mPointLights; }
