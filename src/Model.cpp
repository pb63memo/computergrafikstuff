
#include "Model.hpp"
#include <cmath>
#include <algorithm>
// Konstruktor

#define EPSILON \
  (1e-12)
Model::Model()
{
  /* Aufgabenblatt 2, Aufgabe 3: Setzen Sie die default Werte */

  // es wird um keine Achse gedreht
  mRotation = GLVector(0.0, 0.0, 0.0);

  // es wird nicht verschoben
  mTranslation = GLVector(0.0, 0.0, 0.0);

  // skalierung bleibt auf jeder achse gleich
  mScale = GLVector(1.0, 1.0, 1.0);
}

void Model::applyTransforms()
{
  GLMatrix transformationMatrix = getTransformation();
  for (size_t TriangleID = 0; TriangleID < mTriangles.size(); ++TriangleID)
  {
    Triangle &tri = mTriangles[TriangleID];
    for (int i = 0; i < 3; i++)
    {
      // transformiertes Dreick berechnen
      tri.vertex[i] = transformationMatrix * tri.vertex[i];
    }
    tri.normal = transformationMatrix * tri.normal;
  }
}
// diese funktion läuft eig relativ schnell, verlangsamt aber iwi das programm um das dreifache(dabei hab ich noch nichtmal das shading implementiert)
void Model::fillNeighbors()
{
  int count = 0;
  // std::vector<std::map<int,std::vector<int>>> nachbarn;
  for (size_t i = 0; i < mTriangles.size(); i++)
  {
    Triangle currentTriangle = mTriangles[i];

    std::map<int, std::vector<int>> triangleMap;
    for (size_t j = 0; j < 3; j++)
    {
      std::vector<int> triNeighbors;
      for (size_t k = 0; k < mTriangles.size(); k++)
      {
        Triangle checkTriangle = mTriangles[k];
        for (size_t l = 0; l < 3; l++)
        {
          if (equal(checkTriangle.vertex[l], currentTriangle.vertex[j]))
          {
            triNeighbors.push_back(k);
          }
        }
      }

      triangleMap.insert({j, triNeighbors});
    }
    mNeighbors.push_back(triangleMap);
  }
}

void Model::initOcttree()
{

  std::vector<size_t> hi;
  for (size_t ID = 0; ID < mTriangles.size(); ID++)
  {
    hi.push_back(ID);
  }
  calcOcttree(2, hi, mLowest, mHighest, mOcttree);
}

std::vector<size_t> Model::findTriangles(const Ray &ray, Node &node)
{
  std::vector<size_t> init;
  if (intersectAABB(ray.origin, ray.direction, node.low, node.high))
  {
    init.insert(init.end(), node.ids.begin(), node.ids.end());
  }

  for (size_t i = 0; i < 8; ++i)
  {
    if (node.children[i] == nullptr)
    {
      continue; // Base case: stop recursion if the node is nullptr
    }
    std::vector<size_t> teilvec;
    teilvec = findTriangles(ray, *node.children[i]);
    init.insert(init.end(), teilvec.begin(), teilvec.end());
  }
  return init;
}

void Model::calcOcttree(size_t recursion_depth, std::vector<size_t> ids, GLPoint low, GLPoint high, Node &child)
{
  child.low = low;
  child.high = high;
  child.center = (child.low + child.high) * 0.5;
  if (recursion_depth == 0 || ids.empty())
  {
    child.ids = ids;
    return;
  }
  // std::cout << low << std::endl;

  std::vector<size_t> childIds[8];

  for (size_t id : ids)
  {
    Triangle tri = mTriangles[id];
    int index = (tri.vertex[0](0) > child.center(0)) << 2 |
                (tri.vertex[0](1) > child.center(1)) << 1 |
                (tri.vertex[0](2) > child.center(2));
    // std::cout << index << std::endl;
    for (int j = 1; j < 3; j++)
    {
      int newindex = (tri.vertex[j](0) > child.center(0)) << 2 |
                     (tri.vertex[j](1) > child.center(1)) << 1 |
                     (tri.vertex[j](2) > child.center(2));
      if (!(index == newindex))
      {
        child.ids.push_back(id);
        index = -1;
        break;
      }
    }

    if (index != -1)
    {
      childIds[index].push_back(id);
    }
  }

  for (size_t k = 0; k < 8; k++)
  {
    child.children[k] = new Node();
    Node *kid = child.children[k];
    size_t xMask = 1 << 2;
    size_t yMask = 1 << 1;
    size_t zMask = 1 << 0;

    // Extrahieren der x, y und z Werte
    size_t x = (k & xMask) ? 1 : 0;
    size_t y = (k & yMask) ? 1 : 0;
    size_t z = (k & zMask) ? 1 : 0;

    GLPoint newlow;
    GLPoint newhigh;
    newlow(0) = x ? child.center(0) : low(0);
    newlow(1) = y ? child.center(1) : low(1);
    newlow(2) = z ? child.center(2) : low(2);
    newhigh(0) = x ? high(0) : child.center(0);
    newhigh(1) = x ? high(1) : child.center(1);
    newhigh(2) = x ? high(2) : child.center(2);

    // achtung, hier muss man noch lwo un high berechnen
    calcOcttree(recursion_depth - 1, childIds[k], newlow, newhigh, *kid);
  }
}
// berechnet die Vertexnormalen und auch gleichzeitig die AABB für intersection testing
void Model::calcVertexNormals()
{
  for (int ID = 0; ID < mTriangles.size(); ID++)
  {
    Triangle &tri = mTriangles[ID];
    for (size_t i = 0; i < 3; i++)
    {
      // Boundingbox berechnen
      for (size_t j = 0; j < 3; j++)
      {
        mLowest(j) = std::min(mLowest(j), tri.vertex[i](j));
        mHighest(j) = std::max(mHighest(j), tri.vertex[i](j));
      }

      GLVector normale = GLVector(0, 0, 0);
      auto nachbarn = mNeighbors[ID].find(i);
      std::vector<int> wirkliche_nachbarn = nachbarn->second;
      // für jedes Dreieck welches nachbar zu dieser vertex ist
      std::vector<GLVector> vectors_of_vertex;
      for (size_t j = 0; j < wirkliche_nachbarn.size(); j++)
      {
        Triangle nachbarDreieck = mTriangles[wirkliche_nachbarn[j]];
        GLVector nachbarNormale = nachbarDreieck.normal;
        if (std::find(vectors_of_vertex.begin(), vectors_of_vertex.end(), nachbarNormale) == vectors_of_vertex.end())
        {
          GLVector winkeltest1 = nachbarNormale;
          GLVector winkeltest2 = tri.normal;
          winkeltest1.normalize();
          winkeltest2.normalize();
          // nur nachbarn zählen, welche keine zu scharfen kanten haben
          if (dotProduct(winkeltest1, winkeltest2) > 0 + EPSILON)
          {
            vectors_of_vertex.push_back(nachbarNormale);
          }
        }
        else
        {
          // std::cout<<"duplicate found"<<std::endl;
        }
      }
      for (size_t j = 0; j < vectors_of_vertex.size(); j++)
      {
        normale = normale + vectors_of_vertex[j];
      }

      // normale normieren
      normale.normalize();
      tri.vertex_normal[i] = normale;
    }
  }
  // std::cout << mHighest(0) << std::endl;
}
// Setter für das Material
void Model::setMaterial(Material material)
{
  mMaterial = Material();
  mMaterial.smooth = material.smooth;
  mMaterial.reflection = material.reflection;
  mMaterial.refraction = material.refraction;
  mMaterial.transparency = material.transparency;
  mMaterial.color = Color(material.color.r, material.color.g, material.color.b);
}

/* Aufgabenblatt 2, Aufgabe 3: Implementieren Sie die vier Methoden für die Transformationen hier */

void Model::setRotation(GLVector rotation)
{
  mRotation = rotation;
  updateMatrix();
  mRotation = GLVector(0.0, 0.0, 0.0);
}

void Model::setTranslation(GLVector translation)
{
  mTranslation = translation;
  updateMatrix();
  mTranslation = GLVector(0.0, 0.0, 0.0);
}

void Model::setScale(GLVector scale)
{
  mScale = scale;
  updateMatrix();
  mScale = mScale = GLVector(1.0, 1.0, 1.0);
}

GLMatrix Model::getTransformation() const { return mMatrix; }

Material Model::getMaterial() const { return mMaterial; }

void Model::updateMatrix()
{
  // Rotation * Scale * Tranformation * mMatrix
  // mMatrix = GLMatrix();

  GLMatrix transM;
  GLMatrix scaleM;
  GLMatrix rotX;
  GLMatrix rotY;
  GLMatrix rotZ;

  transM.setColumn(3, mTranslation);

  scaleM.setColumn(0, GLVector(mScale(0), 0.0, 0.0));
  scaleM.setColumn(1, GLVector(0.0, mScale(1), 0.0));
  scaleM.setColumn(2, GLVector(0.0, 0.0, mScale(2)));

  double degToRad = M_PI / 180;
  double xRot = mRotation(0) * degToRad;
  double yRot = mRotation(1) * degToRad;
  double zRot = mRotation(2) * degToRad;

  rotX.setColumn(1, GLVector(0.0, cos(xRot), sin(xRot)));
  rotX.setColumn(2, GLVector(0.0, -sin(xRot), cos(xRot)));

  rotY.setColumn(0, GLVector(cos(yRot), 0.0, -sin(yRot)));
  rotY.setColumn(2, GLVector(sin(yRot), 0.0, cos(yRot)));

  rotZ.setColumn(0, GLVector(cos(zRot), sin(zRot), 0.0));
  rotZ.setColumn(1, GLVector(-sin(zRot), cos(zRot), 0.0));

  mMatrix = rotZ * rotY * rotX * transM * scaleM * mMatrix;
}
