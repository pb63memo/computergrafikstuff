#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <unistd.h>

#include <algorithm>
#include <assimp/Importer.hpp>
#include <cmath>
#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include <random>
#include <chrono>
#include <thread>
#include "Scene.hpp"
#include "SolidRenderer.hpp"
#include "WireframeRenderer.hpp"
#include "math.hpp"

int main(int argc, char **argv)
{
  Color black = Color(0.0, 0.0, 0.0);
  Color white = Color(1.0, 1.0, 1.0);
  Color yellow = Color(1.0, 1.0, 0.0);
  Color red = Color(1.0, 0.0, 0.0);
  Color green = Color(0.0, 1.0, 0.0);
  Color blue = Color(0.0, 0.0, 1.0);

  // Dimensionen des Ergebnisbildes im Konstruktor setzen
  std::shared_ptr<Image> img = std::make_shared<Image>(200, 200);
  // // stuff on mir
  // size_t height = img->getHeight();
  // size_t width = img->getWidth();

  // for (size_t i = 0; i < width; i++)
  // {
  //   for (size_t j = 0; j < height; j++)
  //   {
  //     // UV coordinates :)
  //     img->setValue(i,j,Color((double)i/ (double)width,(double)j/(double)height,0.0));

  //   }

  // }

  // img->setValue(3,4,Color(1.0,1.0,1.0));
  // img->setValue(3,4,Color(1.0,1.0,1.0));

  // Verwendete Modelle festlegen
  std::vector<std::string> path_vector;
  path_vector.push_back(std::string("../data/bunny/bunny_scaled.ply"));
  path_vector.push_back(std::string("../data/basicObjects/cube_scaled.ply"));
  // Erzeuge die Szene mit dem default Konstruktor und lade die Modelle
  auto scene = std::make_shared<Scene>();
  scene->load(path_vector);

  /* Aufgabenblatt 1: Instanziieren Sie einen WireframeRenderer */
  WireframeRenderer wfr(scene, img);

  /* Aufgabenblatt 2, Aufgabe 3: Setzen Sie die Transformationen der Modelle */

  /* Aufgabenblatt 2, Aufgabe 1: Rufen Sie Ihre renderScene-Methode hier auf */

  /* Setup der Camera - Erst ab Aufgabenblatt 3 relevant. */
  // Diese Einstellungen beziehen sich auf den world space
  // Beachten Sie, dass Sie in diesem Praktikum keine explizite Umwandlung in
  // den ViewSpace benötigen, da die Strahen für Raycasting und Raytracing im
  // World space definiert sind. Modelle müssen also lediglich in den World
  // space überführt werden

  /* Aufgabenblatt 3:  kommentieren Sie die Zeilen wieder ein, die eine Kamera erzeugen und zur Scene hinzufügen*/

  auto cam = std::make_shared<Camera>();
  GLPoint eye = GLPoint(0.0, 0.0, 200.0);
  cam->setEyePoint(eye);
  cam->setUp(GLVector(0.0, 1.0, 0.0));
  GLVector viewDirection = GLVector(0.0, 0, -1.0);
  viewDirection.normalize();
  cam->setViewDirection(viewDirection);
  cam->setSize(img->getWidth(), img->getHeight());
  scene->setCamera(cam);

  /* Aufgabenblatt 3: Erzeugen Sie mindestens eine Kugel und fügen Sie diese zur Szene hinzu*/
  Model default_cube;
  Sphere default_sphere;
  scene->addModel(default_cube);
  scene->addModel(default_cube);
  scene->addModel(default_cube);
  scene->addSphere(default_sphere);
  scene->addSphere(default_sphere);

  /* Aufgabenblatt 4: Setzen Sie materialeigenschaften für die Kugelen und die Modelle. Die Materialeigenschaften für eine Darstellung entsprechend der Beispiellösung ist in der Aufgabenstellung gegeben. */
  Material bunny_mat;
  Material first_mat;
  Material sec_mat;
  Material third_mat;
  Material quad_mat;
  Material first_sphere_mat;
  Material second_sphere_mat;

  bunny_mat.color = Color(0, 1, 0);
  bunny_mat.reflection = 1.0;
  first_mat.color = Color(0.9, 0.9, 0.3);
  sec_mat.color = Color(0.9, 0.4, 0.3);
  sec_mat.reflection = 0.0;
  third_mat.color = Color(1, 0.0, 0.0);
  quad_mat.color = Color(0.9, 0.9, 0.9);
  quad_mat.reflection = 0.0;

  first_sphere_mat.color = Color(0, 0, 1);
  first_sphere_mat.reflection = 0.0;
  second_sphere_mat.color = Color(0, 1, 1);
  second_sphere_mat.reflection = 0.0;
  /* Aufgabenblatt 3: (Wenn nötig) Transformationen der Modelle im World space, sodass sie von der Kamera gesehen werden könnnen. Die nötigen Transformationen für eine Darstellung entsprechend der Beispiellösung ist in der Aufgabenstellung gegeben. */
  std::vector<Model> &allModels = scene->getModels();
  std::vector<Sphere> &allSpheres = scene->getSpheres();

  Model &bunny = allModels[0];
  bunny.setMaterial(bunny_mat);

  Model &cube = allModels[1];
  cube.setMaterial(first_mat);

  Model &sec_cube = allModels[2];
  sec_cube.setMaterial(sec_mat);

  Model &tri_cube = allModels[3];
  tri_cube.setMaterial(third_mat);

  Model &quad_cube = allModels[4];
  quad_cube.setMaterial(quad_mat);

  Sphere &first_sphere = allSpheres[0];
  first_sphere.setMaterial(first_sphere_mat);

  Sphere &second_sphere = allSpheres[1];
  second_sphere.setMaterial(second_sphere_mat);

  sec_cube.mTriangles = cube.mTriangles;
  tri_cube.mTriangles = cube.mTriangles;
  quad_cube.mTriangles = cube.mTriangles;

  std::cout << "size: " << allModels.size() << '\n';

  // bunny.setRotation(GLVector(0.0,-90.0,-90.0));
  // bunny.setRotation(GLVector(30.0,0.0,0.0));
  // bunny.setScale(GLVector(30,30,30));
  // bunny.setTranslation(GLVector(40,-50,30));

  bunny.setRotation(GLVector(0.0, 170.0, 0.0));
  bunny.setTranslation(GLVector(0, -10, -30));

  // //bunny.setTranslation(GLVector(30,-100,0));

  cube.setTranslation(GLVector(-100, -50, 0.0));

  sec_cube.setTranslation(GLVector(60, 50, -50));

  tri_cube.setTranslation(GLVector(-80, 10, -100));

  quad_cube.setScale(GLVector(20, 10, 10));
  quad_cube.setTranslation(GLVector(0, 350, 0));

  for (int i = 0; i < quad_cube.mTriangles.size(); i++)
  {
    quad_cube.mTriangles[i].normal = quad_cube.mTriangles[i].normal * -1;
  }

  first_sphere.setPosition(GLPoint(-150, 0, -30));
  first_sphere.setRadius(50);

  second_sphere.setPosition(GLPoint(150, 0, 00));
  second_sphere.setRadius(50);
  /* Stelle materialeigenschaften zur verfügung (Relevant für Aufgabenblatt 4)*/

  /* Aufgabenblatt 4  Fügen Sie ein Licht zur Szene hinzu */
  GLPoint punktlichtquelle = GLPoint(-100, 100, 100);
  scene->addPointLight(punktlichtquelle);

  /* Aufgabenblatt 3: erzeugen Sie einen SolidRenderer (vorzugsweise mir einem shared_ptr) und rufen sie die Funktion renderRaycast auf */
  auto solidRenderer = std::make_shared<SolidRenderer>(scene, img, cam);

  solidRenderer->renderRaycast();
  // Schreiben des Bildes in Datei

  // wfr.renderScene(yellow);

  if (argc > 1)
  {
    img->writeAsPPM(argv[1]);
    std::cout << "Bild mit Dimensionen " << img->getWidth() << "x"
              << img->getHeight() << " in Datei " << argv[1] << " geschrieben."
              << std::endl;
  }
  else
  {
    std::cerr
        << "Fehler: Kein Dateiname angegeben. Es wurde kein Output generiert."
        << std::endl;
  }
  return 0;
}
