#include "SolidRenderer.hpp"
#include <array>
#include <algorithm>
#include <chrono>
// #include <tbb/tbb.h>  // Include, nur wenn TBB genutzt werden soll

#define EPSILON \
  (1e-12) // Epsilon um ungenauigkeiten und Rundungsfehler zu kompensieren

// utah teapot verursacht segfault wenn die maxrecursions mehr als 1 sind
#define MAXRECURSION \
  (2)

#define ANTIALIASING \
  (1)
/**
 ** Erstellt mittels Raycast das Rendering der mScene in das mImage
 ** Precondition: Sowohl mImage, mScene, mCamera müssen gesetzt sein.
 **/
void SolidRenderer::renderRaycast()
{
  for (auto &model : mScene->getModels())
  {

    // transformation am anfang des renders durchführen
    // Apply operations to the model
    model.applyTransforms();
    model.fillNeighbors();
    model.calcVertexNormals();
    model.initOcttree();
    std::cout << model.mNeighbors.size() << std::endl;
    // Other operations on the model
  }

  // This function is part of the base
  float rows = 0;
  float progress = 0;
  int barWidth = 70;

  std::cout << "Rendern mittels Raycast gestartet." << std::endl;
  // Berechnung der einzelnen Rows in eigener Methode um die
  // Parallelisierbarkeit zu verbessern

  // Ohne parallelisierung:

  // for(size_t i = 0; i < mImage->getHeight(); ++i ){
  //         computeImageRow( i );
  //  }

  //  Parallelisierung mit OpenMP:

  float test = 0;
#pragma omp parallel for
  for (size_t i = 0; i < mImage->getHeight(); ++i)
  {
    test += 1;
    std::clog << "\rabgeschlossen: " << (test / mImage->getHeight()) << ' ' << std::flush;
    computeImageRow(i);
    // progress bar destroys reflection :(

    // #pragma omp critical
    // {
    //   rows += 1;
    //   progress = rows/mImage->getHeight();
    //   std::cout << "[";
    //   int pos = barWidth * progress;
    //   for (int i = 0; i < barWidth; ++i) {
    //       if (i < pos) std::cout << "=";
    //       else if (i == pos) std::cout << ">";
    //       else std::cout << " ";
    //   }
    //   std::cout << "] " << int(progress * 100.0) << " %\r";
    //   std::cout.flush();
    // }
  }
  std::cout << "Time it took for intersecting: " << ((mTime / 1e+06)) / 8 << std::endl;
  std::cout << "Tests it took for intersecting: " << (mScene->mTests) / 1e+06 << std::endl;
}

/**
 * Aufgabenblatt 3: Hier wird das Raycasting implementiert. Siehe Aufgabenstellung!
 * Precondition: Sowohl mImage, mScene und mCamera  müssen gesetzt sein.
 */
void SolidRenderer::computeImageRow(size_t rowNumber)
{

  // tests haben ergeben, dass shading sowie das testen auf intersection etwas gleich lang brauchen
  //  weitere tests haben ergeben, dass auch in der shade methode intersect aufgerufen wird
  // daraus lässt sich schlussfolgern, dass die intersect Methode verbessert werden muss
  for (size_t j = 0; j < mImage->getWidth(); ++j)
  {
    Color cum_color = Color(0, 0, 0);
    for (size_t k = 0; k < ANTIALIASING; k++)
    {
      Ray r = mCamera->getRay(j, rowNumber);
      HitRecord hit_record;
      hit_record.color = Color(0, 0, 0);
      hit_record.parameter = -1;
      hit_record.triangleId = -1;
      hit_record.modelId = -1;
      hit_record.sphereId = -1;
      hit_record.recursions = 0;

      bool does_intersect = mScene->intersect(r, hit_record, EPSILON);

      if (does_intersect)
      {

        shade(hit_record);
        cum_color += hit_record.color;
      }
    }
    cum_color /= ANTIALIASING;
    mImage->setValue(j, rowNumber, cum_color);
  }
}

/**
 *  Aufgabenblatt 4: Hier wird das raytracing implementiert. Siehe Aufgabenstellung!
 */
void SolidRenderer::shade(HitRecord &r)
{
  Material mat;
  GLPoint punktlicht = mScene->getPointLights()[0];

  // verwemde ich das überhaupt?
  double angle = 1;
  Color baselight = Color(1, 1, 1);

  // Vector vom schnittpunkt zur Lichtquelle
  GLVector L;
  GLVector N = r.normal;

  GLVector V;
  GLVector R;

  GLVector reflectionvector;

  double diffuse_intensity;

  if (r.sphereId > -1)
  {
    mat = mScene->getSpheres()[r.sphereId].getMaterial();
    r.color = mat.color;
  }
  else if (r.modelId > -1)
  {
    // baryzentrische koordinaten berechnen

    Model model = mScene->getModels()[r.modelId];

    // GLMatrix transformationMatrix = model.getTransformation();

    // phong nur bei modellen mit mindestanzahl dreiecken durchführen
    if (model.mTriangles.size() > 1)
    {
      Triangle tri = model.mTriangles[r.triangleId];
      GLPoint A = tri.vertex[0];
      GLPoint B = tri.vertex[1];
      GLPoint C = tri.vertex[2];
      GLPoint P = r.intersectionPoint;

      double flächeninhalt_gesamt = areaParalellogramm(A, B, C);

      double alpha = areaParalellogramm(P, B, C) / flächeninhalt_gesamt;
      double beta = areaParalellogramm(P, C, A) / flächeninhalt_gesamt;
      double gamma = areaParalellogramm(P, A, B) / flächeninhalt_gesamt;

      // double ges = alpha + beta + gamma;

      // std::cout<<"alpha= "<<alpha<<" beta= "<<beta<<" gamma= "<<gamma<<std::endl;
      // std::cout<<ges<<std::endl;

      // für jede vertex die vertexnormale bestimmen
      // neue normale aus vertexnormalen und baryzentrischen koord berechnen
      N = alpha * tri.vertex_normal[0] + beta * tri.vertex_normal[1] + gamma * tri.vertex_normal[2];
    }

    mat = mScene->getModels()[r.modelId].getMaterial();
  }
  else
  {
    r.color = Color(1, 1, 1);
    return;
  }

  N.normalize();
  L = punktlicht - r.intersectionPoint;
  L.normalize();
  V = -1 * r.rayDirection;
  V.normalize();

  R = 2 * dotProduct(L, N) * N + -1 * L;

  reflectionvector = 2 * dotProduct(V, N) * N + -1 * V;

  r.color = mat.color;
  diffuse_intensity = dotProduct(N, L);
  diffuse_intensity = std::max(diffuse_intensity, 0.0);

  double specular_intensity = pow(dotProduct(R, V), 20);

  double ambient_intensity = 1;

  double general_intensity = 0.4 * diffuse_intensity + 0.2 * specular_intensity + 0.4 * ambient_intensity;

  r.color *= general_intensity;

  // schattenberechnung
  HitRecord shadow_HR;
  shadow_HR.color = Color(0, 0, 0);
  shadow_HR.parameter = -1;
  shadow_HR.triangleId = -1;
  shadow_HR.modelId = -1;
  shadow_HR.sphereId = -1;
  shadow_HR.recursions = 0;
  Ray shadow_ray;

  L = punktlicht - r.intersectionPoint;
  L.normalize();

  shadow_ray.origin = r.intersectionPoint + 10 * EPSILON * L;
  shadow_ray.direction = L;
  auto start = std::chrono::high_resolution_clock::now();
  if (mScene->intersect(shadow_ray, shadow_HR, EPSILON))
  {
    if (shadow_HR.parameter <= (punktlicht - r.intersectionPoint).norm())
    {

      r.color *= 0.5;
    }
  }
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  // std::cout << "Time taken by function: " << duration.count() << " microseconds" << std::endl;
  mTime += duration.count();
  // spiegelungsberechnung
  if (mat.reflection > 0 && r.recursions < MAXRECURSION)
  {

    HitRecord reflect_HR;
    reflect_HR.color = Color(0, 0, 0);
    reflect_HR.parameter = -1;
    reflect_HR.triangleId = -1;
    reflect_HR.modelId = -1;
    reflect_HR.sphereId = -1;
    reflect_HR.recursions = r.recursions + 1;

    Ray reflect_ray;
    reflect_ray.origin = r.intersectionPoint + 10 * EPSILON * reflectionvector;
    reflect_ray.direction = reflectionvector;
    if (mScene->intersect(reflect_ray, reflect_HR, EPSILON))
    {
      shade(reflect_HR);
      Color reflected_color = reflect_HR.color;
      reflected_color *= mat.reflection;
      r.color *= (1 - mat.reflection);
      r.color += reflected_color;
    }
    // white skybox, dont ask
    else
    {
      Color white = Color(1, 1, 1);
      white *= mat.reflection;
      r.color *= (1 - mat.reflection);
      r.color += white;
    }
  }
}
