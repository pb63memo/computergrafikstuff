#pragma once

#include "Image.hpp"
#include "Model.hpp"
#include "Scene.hpp"

class WireframeRenderer {
 public:
  // construct WireframeRenderer. Pointers mImage, mScene are are constructed by
  // passing the pointers image, scene. (pointers image and scene are ivalidated
  // )
  WireframeRenderer(std::shared_ptr<Scene> scene, std::shared_ptr<Image> image)
      : mImage(image), mScene(scene) {}

  void renderScene(Color color);

  void drawBresenhamLine(GLPoint p1, GLPoint p2, Color color);
  void drawFilledCircle(GLPoint p1, double radius, Color color);
  void seedFillArea(GLPoint seed, Color borderColor, Color fillColor);
  void seedFillAreaTwo(GLPoint seed, Color borderColor, Color fillColor);
  void firstOctant(GLPoint p1, GLPoint p2, Color color);
  void secondOctant(GLPoint p1, GLPoint p2, Color color);
  void eigthOctant(GLPoint p1, GLPoint p2, Color color);
  void thirdOctant(GLPoint p1, GLPoint p2, Color color);
  void seedFillAreaWithoutBorderColor(GLPoint seed, Color fillColor);
  void drawAALine(GLPoint p1, GLPoint p2, Color color);
  void WuDrawLine(float x0, float y0, float x1, float y1,Color color);
  std::shared_ptr<Image> mImage;
  std::shared_ptr<Scene> mScene;
};
