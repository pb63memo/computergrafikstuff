
#include "WireframeRenderer.hpp"
#include <cmath>
#include <iostream>
#include <stack>
#include <array>
#include <vector>
#include <tuple>

#include <chrono>


/**
** Zeichnet alle Dreiecke der Scene als Wireframe-Rendering unter Verwendung des
* Bresenham-Algorithmus
** Precondition: Sowohl mImage als auch mScene müssen gesetzt sein.
** (Aufgabenblatt 2 - Aufgabe 1)
**/
void WireframeRenderer::renderScene(Color color) {
  // für jedes Model in mScene
  std::vector<Model> &allModels = mScene->getModels();
  for(Model model: allModels) {
    
    
    GLMatrix transformationMatrix = model.getTransformation();
    
    
    // für jedes Triangle in dem Model
    std::vector<Triangle> allTriangles = model.mTriangles;
    Triangle trois = Triangle();
    
    for(auto tri:allTriangles){
      for(int i = 0;  i < 3; i++){
        
        // zeicheLinie zwischen Vertex und Nachbarvertex
        
        //drawBresenhamLine(tri.vertex[i % 3], tri.vertex[(i+1) % 3],color);
        trois.vertex[i] = transformationMatrix*tri.vertex[i];
      }
      
    
      // für jede vertex in dem Triangle 
      for(int j = 0;  j < 3; j++){
        
        // zeicheLinie zwischen Vertex und Nachbarvertex
        
        //drawBresenhamLine(tri.vertex[i % 3], tri.vertex[(i+1) % 3],color);
        drawAALine(trois.vertex[j],trois.vertex[(j+1)%3],color);
      }



    }
    

}



  // zeichne eine Linie zwischen den beiden Vertecies (z-Koordinate wird eh ignoriert :))
}

/**
** Zeichnet unter Verwendung des Bresenham Algorithmus eine Linie zwischen p1
* und p2 (nutzt x & y Komponente - z Komponente wird ignoriert)
** Precondition: Das mImage muss gesetzt sein.
** (Aufgabenblatt 1 - Aufgabe 2)
**/
void WireframeRenderer::drawBresenhamLine(GLPoint p1, GLPoint p2, Color color) {
  int x = p1(0); int y = p1(1);
  int dx = p2(0) - p1(0); int dy = p2(1) - p1(1);
  if(dx >= 0 and dy >= 0 and dx >= dy){
    //first
    firstOctant(p1,p2,color);
  }
  else if(dx<0 and dy< 0 and dx < dy ){
    //fifth
    firstOctant(p2,p1,color);
  }
  else if (dx >= 0 and dy<0 and dx >= -dy){
    //eigth
    eigthOctant(p1,p2,color);
  }
  else if (dx < 0 and dy >= 0 and dx <= -dy){
    // fourth
    eigthOctant(p2,p1,color);
  }
  else if (dx >= 0 and dy >= 0 and dx  < dy){
    // second
    secondOctant(p1,p2,color);
  }
  else if (dx < 0 and dy < 0 and dx  >= dy){
    // sixth
    secondOctant(p2,p1,color);
  }
  else if (dx < 0 and dy >= 0 and -dx < dy){
    // third
    
    thirdOctant(p1,p2,color);
  }
  else if (dx >= 0 and dy < 0 and -dx >= dy){
    // seventh
    thirdOctant(p2,p1,color);
  }
  else{
    std::cout << "no octant found?";
  }

}

void WireframeRenderer::firstOctant(GLPoint p1, GLPoint p2, Color color){
  int x = p1(0); int y = p1(1);
  int dx = p2(0) - p1(0); int dy = p2(1) - p1(1);
  int e = 2 * dy - dx;
  for(int i = 1; i <= dx; i++){
    mImage->setValue(x,y,color);
    if(e >= 0){
      ++y;
      e -= 2 * dx;
    }
    ++x;
    e += 2*dy;
  }
    mImage->setValue(x,y,color);
}

void WireframeRenderer::secondOctant(GLPoint p1, GLPoint p2, Color color){
  int x = p1(0); int y = p1(1);
  int dx = p2(0) - p1(0); int dy = p2(1) - p1(1);
  int e = 2 * dx - dy;
  for(int i = 1; i <= dy; i++){
    mImage->setValue(x,y,color);
    if(e >= 0){
      ++x;
      e -= 2 * dy;
    }
    ++y;
    e += 2*dx;
  }
    mImage->setValue(x,y,color);
}
void WireframeRenderer::thirdOctant(GLPoint p1, GLPoint p2, Color color){
  int x = p1(0); int y = p1(1);
  int dx = p2(0) - p1(0); int dy = p2(1) - p1(1);
  int e = 2 * dx - dy;
  for(int i = 1; i <= dy; i++){
    mImage->setValue(x,y,color);
    if(e >= 0){
      --x;
      e -= 2 * dy;
    }
    ++y;
    e -= 2*dx;
  }
    mImage->setValue(x,y,color);
}

void WireframeRenderer::eigthOctant(GLPoint p1, GLPoint p2, Color color){
  int x = p1(0); int y = p1(1);
  int dx = p2(0) - p1(0); int dy = p2(1) - p1(1);
  int e = -2 * dy - dx;
  for(int i = 1; i <= dx; i++){
    mImage->setValue(x,y,color);
    if(e >= 0){
      --y;
      e -= 2 * dx;
    }
    ++x;
    e -= 2*dy;
  }
    mImage->setValue(x,y,color);
}

void WireframeRenderer::drawFilledCircle(GLPoint p1, double radius, Color color) {
  // iteriert über die bounding-box des kreises
    for (size_t i = p1(0)-radius; i < p1(0)+radius; i++)
      {
        for (size_t j =p1(1)-radius; j < p1(1)+radius; j++)
        {
          // jeder punkt der innerhalb des kreises liegt wird gezeichnet
            if((std::sqrt(pow((p1(0)-(double)i),2.0) + pow((p1(1)-(double)j),2.0)) < radius) and (std::sqrt(pow((p1(0)-(double)i),2.0) + pow((p1(1)-(double)j),2.0)) > radius-5)){
                mImage->setValue(i,j,color);

            }

        }
        

      }
    
  
}

/**
** Füllt einen vorgegebenen Bereich (abgegrenzt durch Randfarbe/borderColor) mit einer vorgegebenen Farbe (fillColor).
** Precondition: Das mImage muss gesetzt sein.
** (Aufgabenblatt 1 - Aufgabe 3) 
**/

void WireframeRenderer::seedFillArea(GLPoint seed, Color borderColor, Color fillColor) {
  // initialisieren des stacks und erstes element
  std::stack<GLPoint> fillStack;
  fillStack.push(seed);

  //definieren für die nachbarpunkte
  // statt GLPoint lieber indices verwenden (überall)
  std::array<GLPoint,4>  adders = {GLPoint(1.0,0.0,0.0),GLPoint(0.0,-1.0,0.0),GLPoint(0.0,1.0,0.0),GLPoint(-1.0,0.0,0.0)};

  //höhe und breite
  int height = mImage->getHeight();
  int width = mImage->getWidth();
  
  //referenz auf die werte des bildes
  std::vector<Color>&values = mImage->getValues();
  while (!fillStack.empty())
  {
   
    // den obersten pixel färben und entfernen
    GLPoint pixel = fillStack.top();
    fillStack.pop();
    mImage->setValue(pixel(0),pixel(1),fillColor);
   
    //erzeugen der nachbarpixel
    for (GLPoint add : adders)
    {
      GLPoint newPixel = pixel+add;
      
      // had segfault when changing the order of the two if statements because it would first ask for a pixel that might be out of bound
      // thats why it first checks if the pixel is in the scope/in the image
      if(0 <= newPixel(0) && 0 <= newPixel(1) && newPixel(0) < width && newPixel(1) < height ){
        Color oldColor = values[newPixel(1) * width + newPixel(0)];
        if(!(oldColor == borderColor) && !(oldColor == fillColor)){
          fillStack.push(newPixel);
        }
      }
    }
  }

  

  

}

/*statt GL Point verwenden wir hier Integer und indices im stack
  ca. 3 mal schnellere Laufzeit*/
void WireframeRenderer::seedFillAreaTwo(GLPoint seed, Color borderColor, Color fillColor) {
  

  //höhe und breite
  int height = mImage->getHeight();
  int width = mImage->getWidth();

  // initialisieren des stacks und erstes element
  std::stack<int> fillStack;
  int seedint = seed(1) * width + seed(0);
  fillStack.push(seedint);

  //definieren für die nachbarpunkte
  // statt GLPoint lieber indices verwenden (überall)
  std::array<std::tuple<int, int>, 4> adders;

  adders[0] = std::make_tuple(1, 0);
  adders[1] = std::make_tuple(0, 1);
  adders[2] = std::make_tuple(-1, 0);
  adders[3] = std::make_tuple(0, -1);
  //referenz auf die werte des bildes
  std::vector<Color>&values = mImage->getValues();
  while (!fillStack.empty())
  {
   
    // den obersten pixel färben und entfernen
    int pixel = fillStack.top();
    int x = pixel % width;
    int y = pixel / width;
    fillStack.pop();
    mImage->setValue(x,y,fillColor);
  
    //erzeugen der nachbarpixel
    for ( std::tuple<int,int> add : adders)
    {
      int newX = x + std::get<0>(add);
      int newY = y + std::get<1>(add);
      int newIndex = newY * width + newX;
      
      // had segfault when changing the order of the two if statements because it would first ask for a pixel that might be out of bound
      // thats why it first checks if the pixel is in the scope/in the image
      if(0 <= newX && 0 <= newY && newX < width && newY < height ){
        Color oldColor = values[newIndex];
        
        if(!(oldColor == borderColor) && !(oldColor == fillColor)){
          fillStack.push(newIndex);
          
        }
      }
    }
  }

  

  

}


void WireframeRenderer::seedFillAreaWithoutBorderColor(GLPoint seed, Color fillColor) {
  // initialisieren des stacks und erstes element
  std::stack<GLPoint> fillStack;
  
  fillStack.push(seed);

  //definieren für die nachbarpunkte
  std::array<GLPoint,4>  adders = {GLPoint(1.0,0.0,0.0),GLPoint(0.0,-1.0,0.0),GLPoint(0.0,1.0,0.0),GLPoint(-1.0,0.0,0.0)};

  //höhe und breite
  int height = mImage->getHeight();
  int width = mImage->getWidth();
  if(!(0 <= seed(0) and seed(0) <width and 0 <= seed(1) and seed(1) <height)){
    return;
  }
  
  //referenz auf die werte des bildes
  std::vector<Color>&values = mImage->getValues();
  Color seedColor = values[seed(1) * width + seed(0)]; 
  
  while (!fillStack.empty())
  {
   
    // den obersten pixel färben und entfernen
    GLPoint pixel = fillStack.top();
    fillStack.pop();
    mImage->setValue(pixel(0),pixel(1),fillColor);
   
    //erzeugen der nachbarpixel
    for (GLPoint add : adders)
    {
      GLPoint newPixel = pixel+add;
      
      // had segfault when changing the order of the two if statements because it would first ask for a pixel that might be out of bound
      // thats why it first checks if the pixel is in the scope/in the image
      if(0 <= newPixel(0) && 0 <= newPixel(1) && newPixel(0) < width && newPixel(1) < height ){
        Color oldColor = values[newPixel(1) * width + newPixel(0)];
        if((oldColor == seedColor) && !(oldColor == fillColor)){
          fillStack.push(newPixel);
        }
      }
    }
  }

  

}

#include <functional>
#include <algorithm>
#include <utility>

void WireframeRenderer::drawAALine(GLPoint p1, GLPoint p2, Color color){
  WuDrawLine(p1(0),p1(1),p2(0),p2(1),color);
}

void WireframeRenderer::WuDrawLine(float x0, float y0, float x1, float y1, Color color) {
    auto ipart = [](float x) -> int {return int(std::floor(x));};
    auto round = [](float x) -> float {return std::round(x);};
    auto fpart = [](float x) -> float {return x - std::floor(x);};
    auto rfpart = [=](float x) -> float {return 1 - fpart(x);};
        
    const bool steep = abs(y1 - y0) > abs(x1 - x0);
    if (steep) {
        std::swap(x0,y0);
        std::swap(x1,y1);
    }
    if (x0 > x1) {
        std::swap(x0,x1);
        std::swap(y0,y1);
    }
        
    const float dx = x1 - x0;
    const float dy = y1 - y0;
    const float gradient = (dx == 0) ? 1 : dy/dx;
        
    int xpx11;
    float intery;
    {
        const float xend = round(x0);
        const float yend = y0 + gradient * (xend - x0);
        const float xgap = rfpart(x0 + 0.5);
        xpx11 = int(xend);
        const int ypx11 = ipart(yend);
        if (steep) {
            mImage->setAlphaValue(ypx11,     xpx11, rfpart(yend) * xgap,color);
            mImage->setAlphaValue(ypx11 + 1, xpx11,  fpart(yend) * xgap,color);
        } else {
            mImage->setAlphaValue(xpx11, ypx11,    rfpart(yend) * xgap,color);
            mImage->setAlphaValue(xpx11, ypx11 + 1, fpart(yend) * xgap,color);
        }
        intery = yend + gradient;
    }
    
    int xpx12;
    {
        const float xend = round(x1);
        const float yend = y1 + gradient * (xend - x1);
        const float xgap = rfpart(x1 + 0.5);
        xpx12 = int(xend);
        const int ypx12 = ipart(yend);
        if (steep) {
            mImage->setAlphaValue(ypx12,     xpx12, rfpart(yend) * xgap,color);
            mImage->setAlphaValue(ypx12 + 1, xpx12,  fpart(yend) * xgap,color);
        } else {
            mImage->setAlphaValue(xpx12, ypx12,    rfpart(yend) * xgap,color);
            mImage->setAlphaValue(xpx12, ypx12 + 1, fpart(yend) * xgap,color);
        }
    }
        
    if (steep) {
        for (int x = xpx11 + 1; x < xpx12; x++) {
            mImage->setAlphaValue(ipart(intery),     x, rfpart(intery),color);
            mImage->setAlphaValue(ipart(intery) + 1, x,  fpart(intery),color);
            intery += gradient;
        }
    } else {
        for (int x = xpx11 + 1; x < xpx12; x++) {
            mImage->setAlphaValue(x, ipart(intery),     rfpart(intery),color);
            mImage->setAlphaValue(x, ipart(intery) + 1,  fpart(intery),color);
            intery += gradient;
        }
    }
}