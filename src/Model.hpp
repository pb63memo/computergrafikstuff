#pragma once

#include "Material.hpp"
#include "math.hpp"
#include "structs.hpp"
#include <vector>
#include <map>
#include <list>

class Model
{
public:
  Model();

  void setMaterial(Material material);

  // Setter für die Transformationen
  void setRotation(GLVector rotation);
  void setTranslation(GLVector translation);
  void setScale(GLVector scale);
  void applyTransforms();
  void calcVertexNormals();
  void calcAABB();

  // Die Dreiecke aus denen das Modell besteht
  std::vector<Triangle> mTriangles;
  typedef std::vector<std::map<int, std::vector<int>>> Neighbors;

  Neighbors mNeighbors;
  GLPoint mLowest;
  GLPoint mHighest;
  Node mOcttree;

  // Das Material des Modells
  Material getMaterial() const;

  GLMatrix getTransformation() const;

  void fillNeighbors();
  void initOcttree();
  void calcOcttree(size_t recursion_depth, std::vector<size_t> ids, GLPoint low, GLPoint high, Node &child);
  std::vector<size_t> findTriangles(const Ray &ray1, Node &node);

private:
  // Aufgabenblatt 2, Aufgabe 2: Nutzen Sie diese Matrizen für die Transformationen
  GLVector mRotation;
  GLVector mTranslation;
  GLVector mScale;

  Material mMaterial;
  // Die Transformationsmatrix des Modells
  GLMatrix mMatrix;
  void updateMatrix();
};
