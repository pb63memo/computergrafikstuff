#!/bin/bash

# Überprüfe, ob ein Dateiname als Argument übergeben wurde
if [ -z "$1" ]; then
    echo "Bitte einen Dateinamen als Argument angeben."
    exit 1
fi

# Annahme: Du befindest dich im entsprechenden Verzeichnis

# Make aufrufen
make

# CMake aufrufen (angenommen, du möchtest cmake .. ausführen)
cmake ..

# Zeit und das eigentliche Programm aufrufen (angenommen, dass das Programm in der gleichen Ebene wie das Skript liegt)
time ./CGPraktikum ../results/"$1"